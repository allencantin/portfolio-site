var personTotal = 0;
var katMoney = 0;
var allenMoney = 0;


function getValueFromField(fieldName){
    return Number(document.getElementById(fieldName).value);
}

function getKatBreakdown(){
    let katIncome = getValueFromField("KatjaIncome");
    let pTotal = personTotal;
    katMoney = katIncome - personTotal;

    document.getElementById("katMath").innerHTML = "$" + katIncome + " - $" + pTotal + " = $" + katMoney;

    //credit cards
    let discover = getValueFromField("kDiscoverInput");
    let temp = katMoney - discover;
    document.getElementById("katDiscover").innerHTML = "$" + katMoney + " - $" + discover + " = $" + temp;
    katMoney = temp;

    let ulta = getValueFromField("kUltaInput");
    temp = katMoney - ulta;
    document.getElementById("katUlta").innerHTML = "$" + katMoney + " - $ " + ulta + " = $" + temp;
    katMoney = temp;

    let apple = getValueFromField("kAppleInput");
    temp = katMoney - apple;
    document.getElementById("katApple").innerHTML = "$" + katMoney + " - $" + apple + " = $" + temp;
    katMoney = temp;

    let amex = getValueFromField("kAmexInput");
    temp = katMoney - amex;
    document.getElementById("katAmex").innerHTML = "$" + katMoney + " - $" + amex + " = $" + temp;
    katMoney = temp;

    let capone = getValueFromField("kCapOneInput");
    temp = katMoney - capone;
    document.getElementById("katCapOne").innerHTML = "$" + katMoney + " - $" + capone + " = $" + temp;
    katMoney = temp;

    //food
    let food = getValueFromField("kFoodInput");
    temp = katMoney - food;
    document.getElementById("katFood").innerHTML = "$" + katMoney + " - $" + food + " = $" + temp;
    katMoney = temp;

    //gas
    let gas = getValueFromField("kGasInput");
    temp = katMoney - gas;
    document.getElementById("katGas").innerHTML = "$" + katMoney + " - $" + gas + " = $" + temp;
    katMoney = temp;

    document.getElementById("katSavings").innerHTML = "SAVINGS = $" + katMoney;

}

function getAllenBreakdown(){
    let allenIncome = getValueFromField("AllenIncome");
    let pTotal = personTotal;
    allenMoney = allenIncome - personTotal;

    document.getElementById("allenMath").innerHTML = "$" + allenIncome + " - $" + pTotal + " = $" + allenMoney;

    //credit cards
    let discover = getValueFromField("aDiscoverInput")
    let temp = allenMoney - discover;
    document.getElementById("allenDiscover").innerHTML = "$" + allenMoney + " - $" + discover + " = $" + temp;
    allenMoney = temp;

    let chase = getValueFromField("aChaseInput");
    temp = allenMoney - chase;
    document.getElementById("allenChase").innerHTML = "$" + allenMoney + " - $" + chase +" = $" + temp;
    allenMoney = temp;

    //food
    let food = getValueFromField("aFoodInput");
    temp = allenMoney - food;
    document.getElementById("allenFood").innerHTML = "$" + allenMoney + " - $" + food + " = $" + temp;
    allenMoney = temp;

    //gas
    let gas = getValueFromField("aGasInput");
    temp = allenMoney - gas;
    document.getElementById("allenGas").innerHTML = "$" + allenMoney + " - $" + gas + " = $" + temp;
    allenMoney = temp;

    document.getElementById("allenSavings").innerHTML = "SAVINGS = $" + allenMoney;
}

function addIncome(){
    let katIncome = getValueFromField("KatjaIncome");
    let allenIncome = getValueFromField("AllenIncome");
    let total = katIncome + allenIncome;
    document.getElementById("incomeTotal").innerHTML = "$" + total;
}

function addExpenses(){
    let rent = getValueFromField("rent");
    let electric = getValueFromField("electric");
    let internet = getValueFromField("internet");
    let rentInsure = getValueFromField("rentInsure");
    let petInsure = getValueFromField("petInsure");
    let carInsure = getValueFromField("carInsure");
    let total = rent + electric + internet + rentInsure + petInsure + carInsure;
    document.getElementById("expenseTotal").innerHTML = "$" + total;
    personTotal = total / 2;
    document.getElementById("personTotal").innerHTML = "$" + personTotal;
}

function createBlob(data){
    return new Blob([data], {type: "text/plain"});
}

function getMoneyData(){
    let data = document.getElementById("titleInput").value;
    data += "\n-------------------------\n";
    //get number totals
    let incomeTotal = getValueFromField("KatjaIncome") + getValueFromField("AllenIncome");
    let expenseTotal = personTotal * 2;
    //
    data += "Total Income = " + incomeTotal + "\n";
    data += "Total Expenses = " + expenseTotal + "\n";
    data += "Expenses Per Person = " + personTotal + "\n";
    data += "\n-------------------------\n";
    data += "Katja Breakdown\n\n";
    data += "Income - Expenses\n";
    //need to get original kat money
    let theMoney = getValueFromField("KatjaIncome") - personTotal;
    //
    data += theMoney + "\n";
    data += "-     " + getValueFromField("kDiscoverInput") + "   Discover\n";
    data += "-     " + getValueFromField("kUltaInput") + "   Ulta\n";
    data += "-     " + getValueFromField("kAppleInput") + "   Apple\n";
    data += "-     " + getValueFromField("kAmexInput") + "   American Express\n";
    data += "-     " + getValueFromField("kCapOneInput") + "   Capital One\n";
    data += "-     " + getValueFromField("kFoodInput") + "   Food\n";
    data += "-     " + getValueFromField("kGasInput") + "   Gas\n";
    data += "__________\n\n";
    data += " " + katMoney + "  SAVINGS\n"
    data += "\n-------------------------\n";
    data += "Allen Breakdown\n\n";
    data += "Income - Expenses\n";
    //need to get original allen money
    let theMoney2 = getValueFromField("AllenIncome") - personTotal;
    data += "-     " + getValueFromField("aDiscoverInput") + "   Discover\n";
    data += "-     " + getValueFromField("aChaseInput") + "   Chase\n";
    data += "-     " + getValueFromField("aFoodInput") + "   Food\n";
    data += "-     " + getValueFromField("aGasInput") + "   Gas\n";
    data += "__________\n\n";
    data += " " + allenMoney + "  SAVINGS\n"
    return data;
}

function saveToFile(){
    const a = document.createElement("a");
    const file = createBlob(getMoneyData());
    const url = window.URL.createObjectURL(file);
    a.href = url;
    a.download = document.getElementById("titleInput").value + ".txt";
    a.click();
    URL.revokeObjectURL();
}